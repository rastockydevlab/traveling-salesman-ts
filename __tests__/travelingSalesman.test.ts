import { Product, ProductPosition, Point } from '../src/types'

import { APIUtils } from '../src/api'

import { 
    calculateDistance,
    findNearesNeighbor,
    findNearesPosition,
    travelingSalesman,
    travelingSalesman2,
    pickClosestPositions
} from '../src/travelingSalesman'

describe('travelingSalesman', () => {
    const start: Point = {x: 0, y: 0, z: 0}
    
    const startPosition: ProductPosition = {
        positionId: 'start',
        x: 0,
        y: 0,
        z: 0,
        productId: 'start',
        quantity: 1,
    }

    const startProduct: Product = {
        productId: 'start',
        positions: [startPosition]
    }

    const jsonProducts = APIUtils.getProductsAndPositionsJSON()
    

    describe('distance', () => {
        it('should return the distance between two points', () => {
            const a = {
                positionId: 'position-1',
                x: 0,
                y: 0,
                z: 0,
                productId: 'product-1',
                quantity: 1,
            }

            const b = {
                positionId: 'position-2',
                x: 3,
                y: 4,
                z: 0,
                productId: 'product-2',
                quantity: 1,
            }

            expect(calculateDistance(a, b)).toBe(5)
        })
    })

    describe('findNearesNeighbor', () => {
        it('should return the nearest neighbor - small sample', () => {
            // Mock products
            const products = [
                {
                    productId: 'product-1',
                    positions: [{
                        positionId: 'position-1',
                        x: 5,
                        y: 12,
                        z: 13,
                        productId: 'product-1',
                        quantity: 1,
                    }]
                },
                {
                    productId: 'product-2',
                    positions: [
                        {
                            positionId: 'position-2',
                            x: 3,
                            y: 4,
                            z: 12,
                            productId: 'product-2',
                            quantity: 1,
                        },
                        {
                            positionId: 'position-3',
                            x: 35,
                            y: 12,
                            z: 37,
                            productId: 'product-2',
                            quantity: 1,
                        },
                    ]
                },
            ]

            // Initialize productsMap
            const productsMap = new Map<string, Product>()

            productsMap.set(startProduct.productId, startProduct)

            for (const product of products) {
                productsMap.set(product.productId, product)
            }

            // Initialize visited
            const visited = new Set<string>([startProduct.productId])

            const { nextProduct, nextPosition, distance } = findNearesNeighbor(startProduct.positions[0], productsMap, visited)

            expect(nextProduct).toEqual(products[1])
            expect(nextPosition).toEqual(products[1].positions[0])
            expect(distance).toBe(13)
        })

        it('should return the nearest neighbor - medium sample', () => {
            // Mock products
            const products = jsonProducts

            // Initialize productsMap
            const productsMap = new Map<string, Product>()

            productsMap.set(startProduct.productId, startProduct)

            for (const product of products) {
                productsMap.set(product.productId, product)
            }

            // Initialize visited
            const visited = new Set<string>([startProduct.productId])

            const { nextProduct, nextPosition, distance } = findNearesNeighbor(startProduct.positions[0], productsMap, visited)

            expect(nextProduct).toEqual(products[0])
            expect(nextPosition).toEqual(products[0].positions[0])
            expect(distance).toBe(3.1622776601683795)
        })
    })

    describe('travelingSalesman', () => {
        it('should return the shortest path and distance', async () => {
            const products = jsonProducts

            const result = travelingSalesman(products, start)
            console.log('TSP v1', result.pickingOrder, result.distance)

            expect(result.pickingOrder).toEqual(
                [
                    { productId: 'start', positionId: 'start' },
                    { productId: 'product-1', positionId: 'position-31' },
                    { productId: 'product-4', positionId: 'position-120' },
                    { productId: 'product-2', positionId: 'position-241' },
                    { productId: 'product-3', positionId: 'position-124' },
                    { productId: 'product-6', positionId: 'position-126' },
                    { productId: 'product-5', positionId: 'position-216' },
                    { productId: 'product-8', positionId: 'position-11' },
                    { productId: 'product-10', positionId: 'position-230' },
                    { productId: 'product-7', positionId: 'position-55' },
                    { productId: 'product-9', positionId: 'position-325' },
                    { productId: 'start', positionId: 'start' }
                ]
            )

            expect(result.distance).toBe(328.3056783391918)

            expect(result.points).toEqual([
                { x: 0, y: 0, z: 0 },
                { x: 3, y: 1, z: 0 },
                { x: 0, y: 6, z: 0 },
                { x: 3, y: 12, z: 0 },
                { x: 12, y: 6, z: 0 },
                { x: 18, y: 6, z: 0 },
                { x: 18, y: 11, z: 0 },
                { x: 33, y: 0, z: 0 },
                { x: 60, y: 11, z: 0 },
                { x: 75, y: 1, z: 0 },
                { x: 75, y: 1, z: 100 },
                { x: 0, y: 0, z: 0 }
            ])
        })
    })

    describe('improved travelingSalesman', () => {
        it('should pick closes position for each product', async () => {
            const positions = pickClosestPositions(jsonProducts, startPosition)

            expect(positions).toEqual(
                [
                    {
                    positionId: 'position-31',
                    x: 3,
                    y: 1,
                    z: 0,
                    productId: 'product-1',
                    quantity: 13
                    },
                    {
                    positionId: 'position-241',
                    x: 3,
                    y: 12,
                    z: 0,
                    productId: 'product-2',
                    quantity: 12
                    },
                    {
                    positionId: 'position-124',
                    x: 12,
                    y: 6,
                    z: 0,
                    productId: 'product-3',
                    quantity: 9
                    },
                    {
                    positionId: 'position-120',
                    x: 0,
                    y: 6,
                    z: 0,
                    productId: 'product-4',
                    quantity: 9
                    },
                    {
                    positionId: 'position-216',
                    x: 18,
                    y: 11,
                    z: 0,
                    productId: 'product-5',
                    quantity: 14
                    },
                    {
                    positionId: 'position-126',
                    x: 18,
                    y: 6,
                    z: 0,
                    productId: 'product-6',
                    quantity: 15
                    },
                    {
                    positionId: 'position-55',
                    x: 75,
                    y: 1,
                    z: 0,
                    productId: 'product-7',
                    quantity: 2
                    },
                    {
                    positionId: 'position-11',
                    x: 33,
                    y: 0,
                    z: 0,
                    productId: 'product-8',
                    quantity: 10
                    },
                    {
                    positionId: 'position-325',
                    x: 75,
                    y: 1,
                    z: 100,
                    productId: 'product-9',
                    quantity: 7
                    },
                    {
                    positionId: 'position-230',
                    x: 60,
                    y: 11,
                    z: 0,
                    productId: 'product-10',
                    quantity: 7
                    }
                ]
            )
        })

        it('should find nearest position', async () => {
            const positions = pickClosestPositions(jsonProducts, startPosition)
            const positionsMap = new Map<string, ProductPosition>(
                positions.map(position => [position.positionId, position])
            )
            
            const visited = new Set<string>()

            const position = findNearesPosition(startPosition, positionsMap, visited)

            console.log(position)

            expect(position).toEqual(
                {
                    nextPosition: {
                      positionId: 'position-31',
                      x: 3,
                      y: 1,
                      z: 0,
                      productId: 'product-1',
                      quantity: 13
                    },
                    distance: 3.1622776601683795
                  }
            )
        })

        it('should return the shortest path and distance', async () => {
            const products = jsonProducts

            const result = travelingSalesman2(products, start)

            console.log('TSP v2', result.pickingOrder, result.distance)

            expect(result.pickingOrder).toEqual(
                [
                    { productId: 'start', positionId: 'start' },
                    { productId: 'product-1', positionId: 'position-31' },
                    { productId: 'product-4', positionId: 'position-120' },
                    { productId: 'product-2', positionId: 'position-241' },
                    { productId: 'product-3', positionId: 'position-124' },
                    { productId: 'product-6', positionId: 'position-126' },
                    { productId: 'product-5', positionId: 'position-216' },
                    { productId: 'product-8', positionId: 'position-11' },
                    { productId: 'product-10', positionId: 'position-230' },
                    { productId: 'product-7', positionId: 'position-55' },
                    { productId: 'product-9', positionId: 'position-325' },
                    { productId: 'start', positionId: 'start' }
                ]
            )

            expect(result.distance).toBe(328.3056783391918)

            expect(result.points).toEqual([
                { x: 0, y: 0, z: 0 },
                { x: 3, y: 1, z: 0 },
                { x: 0, y: 6, z: 0 },
                { x: 3, y: 12, z: 0 },
                { x: 12, y: 6, z: 0 },
                { x: 18, y: 6, z: 0 },
                { x: 18, y: 11, z: 0 },
                { x: 33, y: 0, z: 0 },
                { x: 60, y: 11, z: 0 },
                { x: 75, y: 1, z: 0 },
                { x: 75, y: 1, z: 100 },
                { x: 0, y: 0, z: 0 }
            ])
        })
    })
})