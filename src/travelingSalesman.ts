import { Product, ProductPosition, PickingOrderItem, Point } from "./types"

import { APIUtils } from "./api"


function calculateDistance(a: ProductPosition, b: ProductPosition): number {
    const dx = a.x - b.x
    const dy = a.y - b.y
    const dz = a.z - b.z

    return Math.sqrt(dx ** 2 + dy ** 2 + dz ** 2)
}

function findNearesNeighbor(
    start: ProductPosition,
    products: Map<string, Product>,
    visited: Set<string>
): { nextProduct: Product | null, nextPosition: ProductPosition | null, distance: number } {
    let nearestDistance = Infinity
    let nextPosition = null
    let nextProduct = null

    // search all product for the closest position
    products.forEach((product) => {
        if (visited.has(product.productId))
            return

        for (const position of product.positions) {
            const distance = calculateDistance(start, position)

            if (distance < nearestDistance) {
                nearestDistance = distance
                nextPosition = position
                nextProduct = product
            }
        }
    })
    
    return {
        nextProduct,
        nextPosition,
        distance: nearestDistance
    }
}

function findNearesPosition(
    start: ProductPosition,
    positions: Map<string, ProductPosition>,
    visited: Set<string>
): { nextPosition: ProductPosition | null, distance: number } {
    let nearestDistance = Infinity
    let nextPosition = null

    positions.forEach(position => {
        if (visited.has(position.productId))
            return

        const distance = calculateDistance(start, position)

        if (distance < nearestDistance) {
            nearestDistance = distance
            nextPosition = position
        }
    })
    
    return {
        nextPosition,
        distance: nearestDistance
    }
}

function travelingSalesman(products: Product[], start: Point) {
    // Set the start position
    const startPosition: ProductPosition = {
        positionId: 'start',
        x: start.x,
        y: start.y,
        z: start.z,
        productId: 'start',
        quantity: 1,
    }

    const startProduct: Product = {
        productId: 'start',
        positions: [startPosition]
    }

    // transform products into a map: key is product.id, value is product
    const productsMap = new Map<string, Product>(
        [startProduct, ...products].map(product => [product.productId, product])
    )

    // Keep track of visited products
    const visited = new Set<string>()

    // Prepare data structures to store the results
    const pickingOrder: PickingOrderItem[] = []

    let totalDistance = 0

    const points: Point[] = []

    // Start searching from the start position
    let current = startPosition

    while (visited.size < productsMap.size) {
        // Find the nearest neighbor
        const { nextProduct, nextPosition, distance } = findNearesNeighbor(current, productsMap, visited)

        if (nextProduct && nextPosition) {
            visited.add(nextProduct.productId)

            pickingOrder.push({
                productId: nextProduct.productId,
                positionId: nextPosition.positionId,
            })

            totalDistance += distance

            // Add the next position to the points array for visualization
            points.push({ 
                x: nextPosition.x,
                y: nextPosition.y,
                z: nextPosition.z
            })


            // Update the current position
            current = nextPosition
        }
    }

    // Go back to the start position
    pickingOrder.push({
        productId: startProduct.productId,
        positionId: startPosition.positionId,
    })

    // Add the distance from the last position to the start position
    totalDistance += calculateDistance(current, startProduct.positions[0])

    // Add the end position to the points array for visualization
    points.push(start)

    return {
        pickingOrder,
        distance: totalDistance,
        points,
    }
}

function travelingSalesman2(products: Product[], start: Point) {
    // Set the start position
    const startPosition: ProductPosition = {
        positionId: 'start',
        x: start.x,
        y: start.y,
        z: start.z,
        productId: 'start',
        quantity: 1,
    }

    // create new graph with only 1 product position for each product
    // using closest distance to start position
    const positions = pickClosestPositions(products, startPosition)

    // transform positions into a map: key is product.id, value is position
    const positionsMap = new Map<string, ProductPosition>(
        [startPosition, ...positions].map(position => [position.productId, position])
    )

    // Keep track of visited positions
    const visited = new Set<string>()

    // Prepare data structures to store the results
    const pickingOrder: PickingOrderItem[] = []

    let totalDistance = 0

    const points: Point[] = []

    // Start searching from the start position
    let current = startPosition

    while (visited.size < positionsMap.size) {
        // Find the nearest neighbor
        const { nextPosition, distance } = findNearesPosition(current, positionsMap, visited)

        if (nextPosition) {
            visited.add(nextPosition.productId)

            pickingOrder.push({
                productId: nextPosition.productId,
                positionId: nextPosition.positionId,
            })

            totalDistance += distance

            // Add the next position to the points array for visualization
            points.push({ 
                x: nextPosition.x,
                y: nextPosition.y,
                z: nextPosition.z
            })


            // Update the current position
            current = nextPosition
        }
    }

    // Go back to the start position
    pickingOrder.push({
        productId: startPosition.productId,
        positionId: startPosition.positionId,
    })

    // Add the distance from the last position to the start position
    totalDistance += calculateDistance(current, startPosition)

    // Add the end position to the points array for visualization
    points.push(start)

    return {
        pickingOrder,
        distance: totalDistance,
        points,
    }
}

function pickClosestPositions(products: Product[], startPosition: ProductPosition): ProductPosition[] {
    const positions: ProductPosition[] = []
    products.forEach(product => {
        // find position that is closest to start
        let closestPosition = null
        let closestDistance = Infinity

        product.positions.forEach(position => {
            const distance = calculateDistance(startPosition, position)
            if (distance < closestDistance) {
                closestDistance = distance
                closestPosition = position
            }
        })
        
        if (closestPosition)
            positions.push(closestPosition)
    })

    return positions
}

export {
    calculateDistance,
    travelingSalesman,
    travelingSalesman2,
    findNearesNeighbor,
    findNearesPosition,
    pickClosestPositions,
}
