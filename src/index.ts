// Set env variables from .env

import 'dotenv/config'
const port = process.env.PORT || 3000


// Setup express with static files and JSON body parser

import express from 'express'
const app = express()

app.use(express.json())
app.use('/visualization', express.static(`${__dirname}/public`))


// Import utils

import { APIUtils } from './api'
import { travelingSalesman, travelingSalesman2 } from './travelingSalesman'


// Routes

app.get('/', (req, res) => {
    res.json({ message: 'API Works!' })
})

// Optimize path using nearest neighbor algorithm
app.post('/optimize-path', async (req, res) => {
    // parse body
    const order = req.body.order

    // get positions from API
    const products = await APIUtils.getProductsAndPositions(order)

    // optimize products
    const { pickingOrder, distance } = travelingSalesman(products, {x: 50, y: 10, z: 60})
    
    res.json({ pickingOrder, distance })
})

// Optimize path using nearest neighbor algorithm - v2
app.post('/optimize-path-2', async (req, res) => {
    // parse body
    const order = req.body.order

    // get positions from API
    const products = await APIUtils.getProductsAndPositions(order)

    // optimize products
    const { pickingOrder, distance } = travelingSalesman2(products, {x: 50, y: 10, z: 60})
    
    res.json({ pickingOrder, distance })
})



// Enpoint for frontent visualization
app.post('/visualize', async (req, res) => {
    // parse body
    const order = req.body.order

    // get products from API
    const products = await APIUtils.getProductsAndPositions(order)

    // optimize products
    const { points, distance } = travelingSalesman(products, {x: 50, y: 10, z: 60})
    
    res.json({products, points})
})


app.listen(port, () => console.log('server is running at localhost:3000'))