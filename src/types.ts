// type for product positions
type ProductPosition = {
    positionId: string
    x: number
    y: number
    z: number
    productId: string
    quantity: number
}

type Product = {
    productId: string
    positions: ProductPosition[]
}

type PickingOrderItem = {
    productId: string
    positionId: string
}

type Point = {
    x: number,
    y: number,
    z: number
}

export {
    Product,
    ProductPosition,
    PickingOrderItem,
    Point
}