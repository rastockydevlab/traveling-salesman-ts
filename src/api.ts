import { Product, ProductPosition } from './types'

import fs from 'fs'

export namespace APIUtils {
    // Fetch products from API
    export async function getProductPositions(productId: string) {
        const response = await fetch(`${process.env.API_URL}/products/${productId}/positions`, {
            headers: {
                'x-api-key': process.env.API_KEY || ''
            }
        })

        if (!response.ok) {
            throw new Error(`Error calling API: ${response.statusText}`)
        }

        // parse response as json
        const data: Product = {
            productId,
            positions: await response.json()
        }

        return data
    }

    // Fetch multiple products from API
    export async function getProductsAndPositions(productsIds: string[]) {
        const products: Product[] = []

        for (const productId of productsIds) {
            const product: Product = await getProductPositions(productId)
            products.push(product)
        }

        return products
    }

    // For developing purposes
    export function getProductsAndPositionsJSON() {
        // read data.json
        const data = fs.readFileSync('data.json', 'utf-8')
        const products = JSON.parse(data)

        return products
    }
}