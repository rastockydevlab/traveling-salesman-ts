import * as THREE from 'three'
import { OrbitControls } from 'three/addons/controls/OrbitControls.js'


const scene = setupScene()


// Setup scene lights and helpers
setupSceneHelpers(scene)


// Buttons
const calculatePathButton = document.getElementById('calculate-path')
const cleanSceneButton = document.getElementById('clean-scene')

const textArea = document.getElementById('order')

calculatePathButton.addEventListener('click', async () => {
    try {
        const orderText = textArea.value
        const order = JSON.parse(orderText)

        cleanScene(scene)
        setupSceneHelpers(scene)

        try {
          const data = await getData(order)

          if (data) {
            renderProductsAndPath(data)
          }

        } catch (error) {
          console.error('Error:', error)
        }
    } catch (error) {
        alert('invalid JSON')
    }
})

cleanSceneButton.addEventListener('click', () => {
    cleanScene(scene)
})


// HELPER FUNCTIONS

async function getData(order) {
  try {
    const response = await fetch('/visualize', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(order),
    })
    const data = await response.json()
    return data
  } catch (error) {
    console.error('Error:', error)
    return null
  }
}

async function getDataMocked() {
    const data = [
        {
          "productId": "product-1",
          "positions": [
            {
              "positionId": "position-31",
              "x": 3,
              "y": 1,
              "z": 0,
              "productId": "product-1",
              "quantity": 13
            },
            {
              "positionId": "position-449",
              "x": 87,
              "y": 7,
              "z": 100,
              "productId": "product-1",
              "quantity": 4
            },
            {
              "positionId": "position-404",
              "x": 42,
              "y": 6,
              "z": 100,
              "productId": "product-1",
              "quantity": 16
            },
            {
              "positionId": "position-282",
              "x": 36,
              "y": 0,
              "z": 100,
              "productId": "product-1",
              "quantity": 12
            },
            {
              "positionId": "position-458",
              "x": 24,
              "y": 10,
              "z": 100,
              "productId": "product-1",
              "quantity": 10
            },
            {
              "positionId": "position-175",
              "x": 75,
              "y": 7,
              "z": 0,
              "productId": "product-1",
              "quantity": 9
            },
            {
              "positionId": "position-397",
              "x": 21,
              "y": 6,
              "z": 100,
              "productId": "product-1",
              "quantity": 6
            }
          ]
        },
        {
          "productId": "product-2",
          "positions": [
            {
              "positionId": "position-241",
              "x": 3,
              "y": 12,
              "z": 0,
              "productId": "product-2",
              "quantity": 12
            },
            {
              "positionId": "position-245",
              "x": 15,
              "y": 12,
              "z": 0,
              "productId": "product-2",
              "quantity": 5
            },
            {
              "positionId": "position-234",
              "x": 72,
              "y": 11,
              "z": 0,
              "productId": "product-2",
              "quantity": 12
            }
          ]
        },
        {
          "productId": "product-3",
          "positions": [
            {
              "positionId": "position-342",
              "x": 36,
              "y": 2,
              "z": 100,
              "productId": "product-3",
              "quantity": 6
            },
            {
              "positionId": "position-720",
              "x": 0,
              "y": 10,
              "z": 200,
              "productId": "product-3",
              "quantity": 9
            },
            {
              "positionId": "position-373",
              "x": 39,
              "y": 5,
              "z": 100,
              "productId": "product-3",
              "quantity": 13
            },
            {
              "positionId": "position-616",
              "x": 48,
              "y": 2,
              "z": 200,
              "productId": "product-3",
              "quantity": 5
            },
            {
              "positionId": "position-632",
              "x": 6,
              "y": 5,
              "z": 200,
              "productId": "product-3",
              "quantity": 15
            },
            {
              "positionId": "position-124",
              "x": 12,
              "y": 6,
              "z": 0,
              "productId": "product-3",
              "quantity": 9
            },
            {
              "positionId": "position-201",
              "x": 63,
              "y": 10,
              "z": 0,
              "productId": "product-3",
              "quantity": 8
            }
          ]
        },
        {
          "productId": "product-4",
          "positions": [
            {
              "positionId": "position-591",
              "x": 63,
              "y": 1,
              "z": 200,
              "productId": "product-4",
              "quantity": 16
            },
            {
              "positionId": "position-260",
              "x": 60,
              "y": 12,
              "z": 0,
              "productId": "product-4",
              "quantity": 7
            },
            {
              "positionId": "position-120",
              "x": 0,
              "y": 6,
              "z": 0,
              "productId": "product-4",
              "quantity": 9
            },
            {
              "positionId": "position-232",
              "x": 66,
              "y": 11,
              "z": 0,
              "productId": "product-4",
              "quantity": 14
            },
            {
              "positionId": "position-630",
              "x": 0,
              "y": 5,
              "z": 200,
              "productId": "product-4",
              "quantity": 3
            }
          ]
        },
        {
          "productId": "product-5",
          "positions": [
            {
              "positionId": "position-578",
              "x": 24,
              "y": 1,
              "z": 200,
              "productId": "product-5",
              "quantity": 5
            },
            {
              "positionId": "position-536",
              "x": 78,
              "y": 12,
              "z": 100,
              "productId": "product-5",
              "quantity": 7
            },
            {
              "positionId": "position-216",
              "x": 18,
              "y": 11,
              "z": 0,
              "productId": "product-5",
              "quantity": 14
            },
            {
              "positionId": "position-647",
              "x": 51,
              "y": 5,
              "z": 200,
              "productId": "product-5",
              "quantity": 15
            },
            {
              "positionId": "position-218",
              "x": 24,
              "y": 11,
              "z": 0,
              "productId": "product-5",
              "quantity": 15
            },
            {
              "positionId": "position-679",
              "x": 57,
              "y": 6,
              "z": 200,
              "productId": "product-5",
              "quantity": 3
            }
          ]
        },
        {
          "productId": "product-6",
          "positions": [
            {
              "positionId": "position-392",
              "x": 6,
              "y": 6,
              "z": 100,
              "productId": "product-6",
              "quantity": 6
            },
            {
              "positionId": "position-577",
              "x": 21,
              "y": 1,
              "z": 200,
              "productId": "product-6",
              "quantity": 20
            },
            {
              "positionId": "position-310",
              "x": 30,
              "y": 1,
              "z": 100,
              "productId": "product-6",
              "quantity": 20
            },
            {
              "positionId": "position-126",
              "x": 18,
              "y": 6,
              "z": 0,
              "productId": "product-6",
              "quantity": 15
            },
            {
              "positionId": "position-312",
              "x": 36,
              "y": 1,
              "z": 100,
              "productId": "product-6",
              "quantity": 15
            }
          ]
        },
        {
          "productId": "product-7",
          "positions": [
            {
              "positionId": "position-55",
              "x": 75,
              "y": 1,
              "z": 0,
              "productId": "product-7",
              "quantity": 2
            },
            {
              "positionId": "position-420",
              "x": 0,
              "y": 7,
              "z": 100,
              "productId": "product-7",
              "quantity": 6
            },
            {
              "positionId": "position-764",
              "x": 42,
              "y": 11,
              "z": 200,
              "productId": "product-7",
              "quantity": 17
            },
            {
              "positionId": "position-418",
              "x": 84,
              "y": 6,
              "z": 100,
              "productId": "product-7",
              "quantity": 18
            },
            {
              "positionId": "position-629",
              "x": 87,
              "y": 2,
              "z": 200,
              "productId": "product-7",
              "quantity": 18
            }
          ]
        },
        {
          "productId": "product-8",
          "positions": [
            {
              "positionId": "position-407",
              "x": 51,
              "y": 6,
              "z": 100,
              "productId": "product-8",
              "quantity": 6
            },
            {
              "positionId": "position-11",
              "x": 33,
              "y": 0,
              "z": 0,
              "productId": "product-8",
              "quantity": 10
            },
            {
              "positionId": "position-199",
              "x": 57,
              "y": 10,
              "z": 0,
              "productId": "product-8",
              "quantity": 9
            },
            {
              "positionId": "position-806",
              "x": 78,
              "y": 12,
              "z": 200,
              "productId": "product-8",
              "quantity": 11
            }
          ]
        },
        {
          "productId": "product-9",
          "positions": [
            {
              "positionId": "position-325",
              "x": 75,
              "y": 1,
              "z": 100,
              "productId": "product-9",
              "quantity": 7
            },
            {
              "positionId": "position-753",
              "x": 9,
              "y": 11,
              "z": 200,
              "productId": "product-9",
              "quantity": 9
            }
          ]
        },
        {
          "productId": "product-10",
          "positions": [
            {
              "positionId": "position-551",
              "x": 33,
              "y": 0,
              "z": 200,
              "productId": "product-10",
              "quantity": 7
            },
            {
              "positionId": "position-283",
              "x": 39,
              "y": 0,
              "z": 100,
              "productId": "product-10",
              "quantity": 17
            },
            {
              "positionId": "position-28",
              "x": 84,
              "y": 0,
              "z": 0,
              "productId": "product-10",
              "quantity": 3
            },
            {
              "positionId": "position-230",
              "x": 60,
              "y": 11,
              "z": 0,
              "productId": "product-10",
              "quantity": 7
            },
            {
              "positionId": "position-805",
              "x": 75,
              "y": 12,
              "z": 200,
              "productId": "product-10",
              "quantity": 1
            }
          ]
        }
    ]

    const result = {
        positions: data,
        points: [
            [ 0, 0, 0 ],
            [ 3, 1, 0 ],
            [ 0, 6, 0 ],
            [ 3, 12, 0 ]
        ]
    }

    return await Promise.resolve(result)
}

function setupScene() {
  const scene = new THREE.Scene()
  const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 1000 )
  camera.position.x = -100
  camera.position.y = 100
  camera.position.z = -100

  const renderer = new THREE.WebGLRenderer()
  renderer.setSize( window.innerWidth, window.innerHeight )
  document.body.appendChild( renderer.domElement )

  const controls = new OrbitControls( camera, renderer.domElement )
  
  // Start animation
  startAnimation(scene, renderer, camera)

  return scene
}

function getRandomColor() {
    // Generate random values for red, green, and blue components
    const red = Math.floor(Math.random() * 256)
    const green = Math.floor(Math.random() * 256)
    const blue = Math.floor(Math.random() * 256)
  
    // Combine the values into a single number using bitwise operators
    const color = (red << 16) + (green << 8) + blue
  
    // Return the color as a number
    return color
}

function cleanScene(scene) {
    while (scene.children.length){
        scene.remove(scene.children[0]);
    }
}

function startAnimation(scene, renderer, camera) {
    function animate() {
        requestAnimationFrame( animate )
        renderer.render( scene, camera )
    }

    animate()
}

function setupSceneHelpers(scene) {
    // Lights

    const ambientLight = new THREE.AmbientLight(0xffffff)

    const pointLight = new THREE.PointLight(0xffffff, 0.2);
    pointLight.position.set(0, 100, 50);

    scene.add(ambientLight)
    scene.add(pointLight);


    // Helpers

    const axesHelper = new THREE.AxesHelper( 300 )
    scene.add(axesHelper)

    const pointLightHelper = new THREE.PointLightHelper(pointLight, 1)
    scene.add(pointLightHelper)
}

function renderProductsAndPath(data) {
  // reuse the same geometry
  const geometry = new THREE.SphereGeometry( 1, 16, 16 )

  data.products.forEach(product => {
      // color each product differently
      const material = new THREE.MeshStandardMaterial( { 
          color: getRandomColor(),
          roughness: 0.5,
          metalness: 0.5
      })

      product.positions.forEach(position => {
          // position
          const sphere = new THREE.Mesh( geometry, material )
          sphere.position.x = position.x
          sphere.position.y = position.z
          sphere.position.z = position.y

          scene.add(sphere)
      })
  })

  // render path
  createLine(scene, data.points)
}

function createLine(scene, points) {
  // Convert points to Vector3
  const vertices = points.map(point => new THREE.Vector3(point.x, point.z, point.y))

  // Create line geometry
  const geometry = new THREE.BufferGeometry().setFromPoints(vertices)

  // Define gradient colors
  const color1 = new THREE.Color(0x0000ff) // Blue
  const color2 = new THREE.Color(0x00ff00) // Green

  // Create color array with gradually shifted colors
  const colors = []
  
  const numPoints = points.length

  for (let i = 0; i < numPoints; i++) {
    const gradient = i / (numPoints - 1) // Calculate gradient between 0 and 1
    const color = new THREE.Color().lerpColors(color1, color2, gradient) // Interpolate color
    colors.push(color.r, color.g, color.b)
  }

  // Create line material with vertex colors
  const material = new THREE.LineBasicMaterial({
    vertexColors: true,
    linewidth: 2
  })

  // Set vertex colors in geometry
  geometry.setAttribute('color', new THREE.Float32BufferAttribute(colors, 3))

  // Create line and add to scene
  const line = new THREE.Line(geometry, material)

  scene.add(line)
}