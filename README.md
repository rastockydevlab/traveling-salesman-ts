# Traveling Salesman in TS

## Getting started

Depends on `NodeJs v18.7.0` and `npm 8.15.0`

1. Clone this repo `git clone https://gitlab.com/rastockydevlab/traveling-salesman-ts.git`
2. Cd into the directory `cd traveling-salesman-ts`
3. Install npm dependencies `npm i`
4. Create `.env` file, copy contents of `example.env` and set `API_URL` and `API_KEY`.
   - Example of `API_URL`: `https://api.url.com/case-study`
5. Start the server `npm run build` and `npm start`
6. Interact with the API
   - Using a simple frontend at `localhost:3000/visualization`
   - Using API Testing tool like Insomnia - create `POST` request with `JSON` body:
```JSON
{
    "order": [
        "product-1",
        "product-2",
        "product-3"
    ]
}
```

![visualize products and path in 3d space](https://gitlab.com/rastockydevlab/traveling-salesman-ts/-/raw/master/visualize.png)

## Understanding the problem

### Input

Array of product ids `["product-1", "product-2", …]`

### Output

Return an array of product positions in order of picking, and the total length of the path in 3d space.

```json
{
    "pickingOrder": [
        { "productId": "product-2", "positionId": "position-123" },
        { "productId": "product-1", "positionId": "position-55" }
    ],
    "distance": 512
}
```

### NOTE: Start and End positions

One thing that's not clear from the specification is:  
what should be the `start` and `end` positions be?

I decided to set the start position manually and make the end position equal to start.

## Process

1. POST to API with JSON body - {order: array of product ids}
1. Query the GymBeam API for positions of products
1. Calculate optimal path
1. Return JSON with picking order and distance

## Finding the shortest path

This is a variation of Traveling Salesman Problem (TSP), where we don't want to visit each node, but each group of nodes.
We only consider the positions of products (points) in 3d space, completely disregarding any and all obstacles (walls, floors, racks…).

![visualize points in space and graph complexity](https://gitlab.com/rastockydevlab/traveling-salesman-ts/-/raw/master/visualization.png)
  
We can look at positions of products as nodes, which are implicitly connected by edges (every node is connected to every other node). This would be an undirected, weighted graph, where the weight of an edge is the Euclidean distance between the 2 points.
  
Finding the shortest path that covers all nodes in a graph is a well-known "Traveling Salesman" problem. If we want to guarantee a perfect solution, we have to use brute-force and calculate all possible paths. This is fine for a small number of nodes, but as the input grows linearly, the time to compute grows exponentially. The complexity is (n)! and even for a small number of nodes (say 10) we have to calculate 10! of possibilities, which is roughly 3.6 * 106 paths. Ouch.
  
For larger graphs we have to make a compromise - in order to calculate a result faster, we will compromise on the quality of the result. We can define a heuristic that will simplify calculation of shortest path, but we will not be able to guarantee the perfect result anymore. We can use "closest neighbor" heuristic - we will always add the closest unvisited node as the next node to visit, until there are no unvisited nodes left. This approach would bring the complexity down to O(n2).
To compare with the brute-force solution: when n=10, O(n2) = 100 while O(n!) = 3 628 800. Much better.
  
Our situation is slightly more complicated. Since nodes are in groups, and we want to visit each group only once, we will keep track of visited groups (product) instead of visited nodes (product position). Example:

- we start at position `{x: 0, y: 0, z: 0}`
- we search through every product and their product positions, until we find the closest product position
- we save this product position and we mark the product this position belongs to as `visited`
- we repeat this process with position found in last step, until every product is marked as `visited`
- we return to the start position

## Next Steps

This approach is not optimal, but it is a great starting place. Path found by nearest neighbour heuristic is a good candidate for further optimization using 2-opt or 3-opt algorithm, or even using a genetic algorithm.

Also, for this solution to be practical we have to take obstacles into account.
